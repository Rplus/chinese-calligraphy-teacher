module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        stylus: {
            compile: {
                option: {
                    // urlfunc: 'embedurl'
                },
                files: {
                    './assets/style/style.css': './assets/style/stylus/style.stylus'
                }
            }
        },

        connect: {
            server: {
                options: {
                    // protocol: 'https'
                }
            }
        },

        cssmin: {
            minify: {
                files: {
                    './assets/style/style.css': './assets/style/style.css'
                }
            }
        },

        watch: {
            stylus: {
                files: ['./assets/style/stylus/*.stylus', './assets/**.html'],
                tasks: ['stylus'],
                options: {
                    livereload: true
                }
            }
        }
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task.
    grunt.registerTask('default', ['stylus', 'cssmin']);
    grunt.registerTask('see', ['connect', 'watch']);
    grunt.registerTask('build', ['stylus', 'cssmin']);
};